<?php 
require_once 'Fight.php';
/**
 * 
 */
class Harimau extends Hewan
{
	use Fight;
	
	function __construct($nama)
	{
		$this->nama = $nama;
		$this->jumlahKaki = 4;
		$this->keahlian = "lari cepat";
		$this->attackPower = 7;
		$this->defencePower = 8;
	}

	public function getInfoHewan()
	{
		echo "Jenis Hewan : Harimau <br>";
		echo "Nama : $this->nama <br>";
		echo "Darah : $this->darah <br>";
		echo "Jumlah Kaki : $this->jumlahKaki <br>";
		echo "Keahlian : $this->keahlian <br>";
		echo "Attack Power : $this->attackPower <br>";
		echo "Defence Power : $this->defencePower <br>";
	}
}

 ?>