<?php 
require_once 'Fight.php';

/**
 * 
 */
class Elang extends Hewan
{
	use Fight;
	
	function __construct($nama)
	{
		$this->nama = $nama;
		$this->jumlahKaki = 2;
		$this->keahlian = "terbang tinggi";
		$this->attackPower = 10;
		$this->defencePower = 5;
	}

	public function getInfoHewan()
	{
		echo "Jenis Hewan : Elang <br>";
		echo "Nama : $this->nama <br>";
		echo "Darah : $this->darah <br>";
		echo "Jumlah Kaki : $this->jumlahKaki <br>";
		echo "Keahlian : $this->keahlian <br>";
		echo "Attack Power : $this->attackPower <br>";
		echo "Defence Power : $this->defencePower <br>";
	}
}

 ?>
